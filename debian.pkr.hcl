#packer  build -on-error=cleanup -var-file=private_variables.pkrvar.hcl debian.pkr.hcl
#  -only=build-debian11-from-iso.virtualbox-iso.debian11

variable "boot_command" {
  type    = string
  default = "<esc><wait>auto preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg<enter>"
}

variable "boot_wait" {
  type    = string
  default = "10s"
}

variable "debian11_iso_checksum" {
  type    = string
  default = "e482910626b30f9a7de9b0cc142c3d4a079fbfa96110083be1d0b473671ce08d"
}

variable "debian11_iso_url" {
  type    = string
  default = "https://cdimage.debian.org/debian-cd/11.6.0/amd64/iso-cd/debian-11.6.0-amd64-netinst.iso"
}

variable "disk_size" {
  type    = string
  default = "8192"
}

variable "headless" {
  type    = bool
  default = false
}

variable "http_directory" {
  type    = string
  default = "http"
}

variable "memsize" {
  type    = string
  default = "1024"
}

variable "numvcpus" {
  type    = string
  default = "1"
}

variable shutdown_command {
  type    = string
  default = "echo 'packer'|sudo -S shutdown -P now"
}

variable "ssh_password" {
  type      = string
  sensitive = true
}

variable ssh_port {
  type    = number
  default = 22
}

variable ssh_timeout {
  type    = string
  default = "30m"
}

variable "ssh_username" {
  type    = string
  default = "packer"
}

variable "debian11_vm_name" {
  type    = string
  default = "debian-11-amd64"
}

source "hyperv-iso" "debian11" {
  boot_command     = ["${var.boot_command}"]
  boot_wait        = "${var.boot_wait}"
  disk_size        = "${var.disk_size}"
  headless         = "${var.headless}"
  http_directory   = "${var.http_directory}"
  iso_checksum     = "${var.debian11_iso_checksum}"
  iso_url          = "${var.debian11_iso_url}"
  shutdown_command = "${var.shutdown_command}"
  ssh_password     = "${var.ssh_password}"
  ssh_port         = "${var.ssh_port}"
  ssh_timeout      = "${var.ssh_timeout}"
  ssh_username     = "${var.ssh_username}"
  vm_name          = "${var.debian11_vm_name}"
  generation       = "1"
  skip_export      = true
  switch_name      = "Default Switch"
}

source "virtualbox-iso" "debian11" {
  boot_command     = ["${var.boot_command}"]
  boot_wait        = "${var.boot_wait}"
  disk_size        = "${var.disk_size}"
  headless         = "${var.headless}"
  http_directory   = "${var.http_directory}"
  iso_checksum     = "${var.debian11_iso_checksum}"
  iso_url          = "${var.debian11_iso_url}"
  shutdown_command = "${var.shutdown_command}"
  ssh_password     = "${var.ssh_password}"
  ssh_port         = "${var.ssh_port}"
  ssh_timeout      = "${var.ssh_timeout}"
  ssh_username     = "${var.ssh_username}"
  vm_name          = "${var.debian11_vm_name}"
  guest_os_type    = "Debian_64"
  vboxmanage = [
    ["modifyvm", "{{ .Name }}", "--memory", "${var.memsize}"],
    ["modifyvm", "{{ .Name }}", "--cpus", "${var.numvcpus}"],
    ["modifyvm", "{{ .Name }}", "--vram", "20"],
    ["modifyvm", "{{ .Name }}", "--graphicscontroller", "vmsvga"],
    ["modifyvm", "{{ .Name }}", "--acpi", "on"],
    ["modifyvm", "{{ .Name }}", "--ioapic", "on"],
    ["modifyvm", "{{ .Name }}", "--rtcuseutc", "on"]
  ]
}

source "vmware-iso" "debian11" {
  boot_command     = ["${var.boot_command}"]
  boot_wait        = "${var.boot_wait}"
  disk_size        = "${var.disk_size}"
  headless         = "${var.headless}"
  http_directory   = "${var.http_directory}"
  iso_checksum     = "${var.debian11_iso_checksum}"
  iso_url          = "${var.debian11_iso_url}"
  shutdown_command = "${var.shutdown_command}"
  ssh_password     = "${var.ssh_password}"
  ssh_port         = "${var.ssh_port}"
  ssh_timeout      = "${var.ssh_timeout}"
  ssh_username     = "${var.ssh_username}"
  vm_name          = "${var.debian11_vm_name}"
  disk_type_id     = "0"
  guest_os_type    = "debian10-64"
  vmx_data = {
    memsize             = "${var.memsize}"
    numvcpus            = "${var.numvcpus}"
    "virtualHW.version" = "14"
  }
}

build {
  name = "build-debian11-from-iso"
  sources = [
    "source.hyperv-iso.debian11",
    "source.virtualbox-iso.debian11",
    "source.vmware-iso.debian11"
  ]

  provisioner "ansible" {
    playbook_file = "scripts/setup.yml"
  }

}
